package wk10;

import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        System.out.println(words.add("pizza"));
        words.add("fun");
        words.add("free");
        words.add("banana");
        words.add("pizza");
        ArrayList<String> words2 = rearrange(words, 0, 4);
        System.out.println(words2);
    }

    private static double averageLength(ArrayList<String> words) {
        int sum = 0;
        for(String word : words) {
            sum += word.length();
        }
        return (double)sum/words.size();
    }

    private static ArrayList<String> rearrange(ArrayList<String> words, int guyToMove,
                                               int finalRestingSpot) {
        ArrayList<String> answer = new ArrayList<>(words);
        String guy = answer.remove(guyToMove);
        answer.add(finalRestingSpot, guy);
        return answer;
    }
    private static String capitalize(String word) {
        return word.substring(0,1).toUpperCase() + word.substring(1);
    }

    private static void cap(ArrayList<String> words) {
        for(int i=0; i<words.size(); ++i) {
            words.set(i, capitalize(words.get(i)));
        }
    }
}

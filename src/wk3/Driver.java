package wk3;

import javax.swing.*;

// if (CONDITIONAL) {
//    STATEMENTS
// }
public class Driver {
    public static void main(String[] args) {
        System.out.println("Enter two integers");
        String first = JOptionPane.showInputDialog("Enter an integer");
        String second = JOptionPane.showInputDialog("Enter another integer");
        int a = Integer.parseInt(first);
        int b = Integer.parseInt(second);
        boolean atLeastOneOdd = (a%2!=0 || b%2!=0);
        boolean firstOdd = a%2!=0;
        boolean secondOdd = b%2!=0;
        atLeastOneOdd = firstOdd || secondOdd;
        boolean bothEven = a%2==0 && b%2==0;

        if(!bothEven) {
            JOptionPane.showMessageDialog(null, "At least one is odd");
        } else {
            JOptionPane.showMessageDialog(null, "Neither is odd");
        }
    }
}




package wk3;

import java.util.Scanner;

public class Exercise {
    // Exercise 8 from msoe.us/taylor/se1011/wk3-3
    public static void main(String[] args) {
        // PRINT "Enter a bunch of positive integers followed by a non-positive integer"
        System.out.println("Enter a bunch of positive integers followed by a non-positive integer");
        // INPUT number
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        // set sum = 0
        int sum = 0;
        // while number>0
        while(number>0) {
        //    if number divisible by 3
            if(number%3==0) {
        //        add number to sum
                sum += number;
            }
        //    INPUT number
            number = in.nextInt();
        }
        //    PRINT sum
        System.out.println("Answer: " + sum);
    }
}

package wk8;

import wk6.Complex;

import java.util.Scanner;

public class Driver {
    public static void main2(String[] args) {
        String[] words = {"thl", "end", null, null, "Really the end"};
        words = new String[2];
        Scanner in = new Scanner(System.in);
        System.out.println("How many quizzes have you taken?");
        int numberOfQuizzes = in.nextInt();
        int[] quizScores = new int[numberOfQuizzes];
        System.out.println("Enter your scores");
        for(int i=0; i<quizScores.length; ++i) {
            quizScores[i] = in.nextInt();
        }
        int min = Integer.MAX_VALUE;
        int sum = 0;
        for(int i=0; i<quizScores.length; ++i) {
            if(min>quizScores[i]) {
                min = quizScores[i];
            }
            sum += quizScores[i];
        }
        sum -= min;
        System.out.println((double)sum/(quizScores.length-1));

    }

    public static void main(String[] args) {
        int numberCorrect = 0;
        Scanner in = new Scanner(System.in);
        Question[] problems = getProblems(4);
        String wrong = "";
        for(int i=0; i<problems.length; i++) {
            if(!quizUser(in, problems, i)) {
                wrong += i + " ";
            }
        }
        System.out.println("You blew it on " + wrong);
        Scanner probNumbers = new Scanner(wrong);
        while(probNumbers.hasNextInt()) {
            int number = probNumbers.nextInt();
            System.out.println(problems[number]);
        }
    }

    private static Question[] getProblems(int numberOfProblems) {
        Question[] problems = new Question[numberOfProblems];
        for(int i=0; i<problems.length; ++i) {
            problems[i] = new Question('+');
        }
        return problems;
    }

    private static boolean quizUser(Scanner in, Question[] problems, int index) {
        System.out.println(problems[index] + " = " + problems[index].getAnswer());
        System.out.println("Enter the answer");
        Complex ans = new Complex(in.nextLine());
        System.out.println(ans);
        return problems[index].isCorrect(ans);
    }

}

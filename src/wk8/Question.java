package wk8;

import wk6.Complex;

import java.util.Random;

public class Question {
    private final Complex first;
    private final Complex second;
    private final char operator;
    private static final Random generator = new Random();


    public Question(char operator) {
        first = new Complex(getRandomDouble(0), getRandomDouble(-2));
        second = new Complex(getRandomDouble(0), getRandomDouble(-2));
        this.operator = operator;
    }


    public String toString() {
        return first + " " + operator + " " + second;
    }


    public Complex getAnswer() {
        Complex answer = null;
        switch(operator) {
            case '+':
                answer = first.plus(second);
                break;
            case '-':
                answer = first.minus(second);
                break;
            case '*':
                answer = first.times(second);
                break;
            case '/':
                answer = first.dividedBy(second);
                break;
        }
        return answer;
    }


    public boolean isCorrect(Complex answer) {
        return answer.equals(getAnswer());
    }


    private static double getRandomDouble(int offset) {
        return generator.nextInt(500)/100.0 + offset;
    }
}

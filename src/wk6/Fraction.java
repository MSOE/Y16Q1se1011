package wk6;

/**
 * Represents a mathematical fraction.
 */
public class Fraction {
    /**
     * Fraction numerator
     */
    private int num;

    /**
     * Fraction denominator
     */
    private int den;

    private static boolean displayAsDecimal = false;

    private static final boolean IS_COOL = true;

    public static boolean toggleDisplayMethod() {
        displayAsDecimal = !displayAsDecimal;
        return displayAsDecimal;
    }

    public Fraction(int numerator, int denominator) {
        num = numerator;
        den = denominator;
        reduce();
    }

    public Fraction(int numerator) {
        this(numerator, 1);
        //num = numerator;
        //den = 1;
    }

    public Fraction plus(Fraction that) {
        int numer = this.num * that.den + that.num * this.den;
        int denom = this.den * that.den;
        return new Fraction(numer, denom);
    }

    public Fraction plus(int that) {
        return new Fraction(that*den+num, den);
    }

    public Fraction minus(Fraction that) {
        that.num *= -1;
        Fraction result = this.plus(that);
        that.num *= -1;
        return result;
        /*
        int numer = this.num * that.den - that.num * this.den;
        int denom = this.den * that.den;
        return new Fraction(numer, denom);
        */
    }

    public String toString() {
        String result = num + "/" + den;
        if(displayAsDecimal) {
            result = Double.toString((double) num / den);
        }
        return result;
    }
    private void reduce() {
        if(den<0) {
            num *= -1;
            den = -den;
        }
        // Magic stuff happens here
    }

    public static void main(String[] args) {
        Fraction f = new Fraction(-3, -8);
        Fraction g = new Fraction(3);
        Fraction.toggleDisplayMethod();
        f.toggleDisplayMethod();
        System.out.println(f + " + " + g
            + " = " + f.plus(8));
    }
}












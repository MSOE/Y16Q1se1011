package wk9;

import java.io.PrintStream;
import java.util.ArrayList;

public class DriverList {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int i=0; i<5; ++i) {
            list.add(i*i);
        }
        list.add(2);

        System.out.println(list);
        System.out.println(list.get(2));

    }

    private static int[] addToEnd(int[] small, int newbie) {
        int[] bigger = new int[small.length+1];
        for(int i=0; i<small.length; ++i) {
            bigger[i] = small[i];
        }
        bigger[bigger.length-1] = newbie;
        return bigger;
    }

    private static void displayIntegers(int[] objects, PrintStream out) {
        for(int i=0; i<objects.length; ++i) {
            out.println(objects[i]);
        }
    }
}









package wk9;

import java.io.PrintStream;

public class Driver {
    public static void main(String[] args) {
        int[] primitives = new int[5];
        for(int i=0; i<primitives.length; ++i) {
            primitives[i] = i*i;
        }

        primitives = addToEnd(primitives, 2);

        displayIntegers(primitives, System.out);
    }

    private static int[] addToEnd(int[] small, int newbie) {
        int[] bigger = new int[small.length+1];
        for(int i=0; i<small.length; ++i) {
            bigger[i] = small[i];
        }
        bigger[bigger.length-1] = newbie;
        return bigger;
    }

    private static void displayIntegers(int[] objects, PrintStream out) {
        for(int i=0; i<objects.length; ++i) {
            out.println(objects[i]);
        }
    }
}









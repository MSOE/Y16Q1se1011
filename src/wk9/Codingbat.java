package wk9;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Codingbat {
    public static void main(String[] args) {
        ArrayList<Integer> inpt = new ArrayList<>();
        inpt.add(1);
        inpt.add(2);
        inpt.add(3);
        int[] input = {1, 2, 3};
        int expected = 6;
        int answer = sum3(input);
        checkAnswer(expected, answer);
    }

    public static int sum3(ArrayList<Integer> nums) {
        int sum = 0;
        for(int num : nums) {
            sum += num;
        }
        return sum;
    }

    public static int sum3(int[] nums) {
        int sum = 0;
        for(int num : nums) {
            sum += num;
        }
/*        for(int i=0; i<nums.length; ++i) {
            int num = nums[i];
            sum += num;
        }
*/
        return sum;
    }

    public static void checkAnswer(int expected, int actual) {
        if(expected!=actual) {
            System.err.println("Wrong!");
        }
    }
}







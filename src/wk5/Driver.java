package wk5;

public class Driver {
    public static void main(String[] args) {
        Mouse george = new Mouse(2);
        george.setGrowthRate(1.2);
        george.getOlder(3);
        george.getOlder();
        george.getOlder();
        george.getOlder();
        george.display();
        george.daysOld = -30;
        System.out.println(george.daysOld);
        System.out.println(george.getAge());
    }
}

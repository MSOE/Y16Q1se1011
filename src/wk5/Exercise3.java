package wk5;

import java.util.Random;

public class Exercise3 {
    public static void main(String[] args) {
        Random generator = new Random();
        // An integer value between 0 and 15 (including 0, but no including 15)
        System.out.println(generator.nextInt(15));
        // An integer value between 5 and 10
        System.out.println(generator.nextInt(5)+5);
        // A double value between 0.0 and 1.0
        System.out.println(generator.nextDouble());
        // A double value between 0.0 and 100.0
        System.out.println(100*generator.nextDouble());
        // A double value between -100.0 and 100.0
        System.out.println(200*generator.nextDouble()-100);
    }
}

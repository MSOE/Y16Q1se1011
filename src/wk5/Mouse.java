package wk5;

public class Mouse {
    private double growthRate;
    private double weight;
    public int daysOld;

    public Mouse(double oz) {
        weight = oz;
        growthRate = 1.0;
        daysOld = 0;
    }

    public void setGrowthRate(double growthRate) {
        this.growthRate = growthRate;
    }

    public void getOlder() {
        weight *= growthRate;
        daysOld += 1;
    }

    public int getAge() {
        return daysOld;
    }

    public void display() {
        System.out.println("I am " + daysOld + " days old."
        + " and growth " + Math.round((growthRate - 1)*100) + " percent each day.");
    }

    public void getOlder(int days) {
        for(int i=0; i<days; ++i) {
            getOlder();
        }
    }
}

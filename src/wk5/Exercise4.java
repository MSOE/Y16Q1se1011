package wk5;

public class Exercise4 {
    public static void main(String[] args) {
        problem7(100.135, -3.7, 70003.888, 23.456, 3.142);
    }

    public static void problem7(double a, double b, double c,
                                double d, double e) {
        String pattern = "%12.3f%n";
        System.out.printf(pattern, a);
        System.out.printf(pattern, b);
        System.out.printf(pattern, c);
        System.out.printf(pattern, d);
        System.out.printf(pattern, e);
        // Alternative approach below
        System.out.printf("%12.3f%n%12.3f%n%12.3f%n%12.3f%n%12.3f%n", a, b, c, d, e);
    }
}
